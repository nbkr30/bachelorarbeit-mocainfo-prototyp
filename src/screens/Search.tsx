import React, { useEffect, useContext } from 'react'
import { RNSearchbar } from '../components/RNSearchbar'
import { View } from 'react-native'
import { Loading } from '../components/Loading'
import { SearchResult } from './../components/SearchResult'
import { MainStackNavProps } from '../routes/MainStack/MainStackParamList'
import { ScrollView } from 'react-native-gesture-handler'
import { BuildingContext } from '../providers/BuildingProvider'
import { PersonContext } from './../providers/PersonProvider'

export const Search = ({ navigation }: MainStackNavProps<'Search'>) => {
    const { buildings, getBuildings } = useContext(BuildingContext)
    const { persons, getPersons } = useContext(PersonContext)
    // objects that matches the text in the searchbar
    const [matches, setMatches] = React.useState<any>(null)

    useEffect(() => {
        buildings ? null : getBuildings()
        persons ? null : getPersons()
    }, [])

    const getMatches = (searchString: string) => {
        var matches = []
        // go through every building and room and if the name of the building or the room somehow matches the searchString push it to the matches array.
        if (buildings) {
            for (var i = 0; i <= buildings.content.length - 1; i++) {
                const currentBuilding = buildings.content[i]
                if (currentBuilding.name) {
                    currentBuilding.name
                        .toLowerCase()
                        .includes(searchString.toLowerCase())
                        ? matches.push({
                              room: null,
                              level: 0,
                              building: currentBuilding,
                              type: 'building',
                              person: null,
                          })
                        : null
                }
                for (var j = 0; j <= currentBuilding.levels.length - 1; j++) {
                    const currentLevel = currentBuilding.levels[j]
                    for (var k = 0; k <= currentLevel.rooms.length - 1; k++) {
                        const currentRoom = currentLevel.rooms[k]
                        if (currentRoom.name) {
                            currentRoom.name
                                .toLowerCase()
                                .includes(searchString.toLowerCase())
                                ? matches.push({
                                      room: currentRoom,
                                      level: j,
                                      building: currentBuilding,
                                      type: 'room',
                                      person: null,
                                  })
                                : null
                        }
                    }
                }
            }
        }
        // go through every persons first and last name and if it matches the searchString push it to the matches array.
        if (persons) {
            for (var i = 0; i <= persons.length - 1; i++) {
                const person = persons[i]
                var name = ''
                if (person.firstName) {
                    name = person.firstName + ' '
                }
                if (person.lastName) {
                    name = name + person.lastName
                }
                // building Backend offers no office names so at the moment it's not possible to view an persons office
                name.toLowerCase().includes(searchString.toLowerCase())
                    ? matches.push({
                          room: null,
                          person: { person: person, name: name },
                          level: -1,
                          building: null,
                          type: 'person',
                      })
                    : null
            }
        }
        setMatches(matches)
    }

    const renderMatches = (matchedRooms: any[]) => {
        const items = []

        for (let i = 0; i <= matchedRooms.length - 1; i++) {
            items.push(renderMatch(matchedRooms[i]))
        }

        return <ScrollView>{items}</ScrollView>
    }

    const renderMatch = (match: any) => {
        return (
            <SearchResult
                onPress={() => {
                    // building Backend offers no office names so at the moment it's not possible to view an persons office
                    match.type === 'person'
                        ? null
                        : navigation.navigate('MapTabs', {
                              screen: 'Map',
                              params: { search: match, showCampus: false },
                          })
                }}
                key={
                    match.type === 'room'
                        ? match.room.name
                        : match.type === 'building'
                        ? match.building.name
                        : match.type === 'person'
                        ? match.person.person.uuid
                        : null
                }
                result={match}
            />
        )
    }

    return buildings && persons ? (
        <>
            <RNSearchbar
                onChangeText={(searchString) => {
                    if (searchString === '') {
                        setMatches(null)
                    } else {
                        getMatches(searchString)
                    }
                }}
                onFocusBar={() => {}}
                onIconPress={() => {
                    navigation.goBack()
                }}
                isFocused={true}
                showIcon={true}
            />

            {matches ? (
                <View style={{ marginTop: 10 }}>{renderMatches(matches)}</View>
            ) : null}
        </>
    ) : (
        <Loading />
    )
}
