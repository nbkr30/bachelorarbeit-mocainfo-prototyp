import React, { useEffect, ReactNode, useContext } from 'react'
import { StyleSheet, View, Text } from 'react-native'
import MapView, { Polygon } from 'react-native-maps'
import { THMCoordinates } from '../constants/constants'
import { Loading } from '../components/Loading'
import { LevelIndicator } from '../components/LevelIndicator'
import { IndoorCamera } from '../constants/constants'
import { RNSearchbar } from '../components/RNSearchbar'
import { FABIconButton } from '../components/FABIconButton'
import { MainStackNavProps } from '../routes/mainStack/MainStackParamList'
import { RNBottomSheet } from '../components/RNBottomSheet'
import * as Location from 'expo-location'
import { BuildingContext } from '../providers/BuildingProvider'

export const Map = ({ navigation, route }: MainStackNavProps<'MapTabs'>) => {
    const { buildings, getBuildings } = useContext(BuildingContext)
    const routeParams = route.params
    // current user location or 'access denied' if the user denied the location access
    const [location, setLocation] = React.useState<null | string | any>(null)
    // current building object for the indoor view
    const [building, setBuilding] = React.useState<any>(null)
    // current room if the user has pressed one in the inndor view
    const [room, setRoom] = React.useState<any>(null)
    const [isButtomSheetOpen, setIsButtomSheetOpen] = React.useState<any>(false)
    // current level of the building if the user is in the indoor view
    const [currentLevel, setCurrentLevel] = React.useState<number>(-1)
    var _mapView: MapView | null

    // if not allready done get the building data
    useEffect(() => {
        buildings ? null : getBuildings()
    }, [])

    // ask the user for the permission to access the location
    useEffect(() => {
        ;(async () => {
            let { status } = await Location.requestPermissionsAsync()
            if (status !== 'granted') {
                setLocation('Access Denied')
            } else {
                let location = await Location.getCurrentPositionAsync({})
                setLocation(location)
            }
        })()
    })

    useEffect(() => {
        const search = routeParams.search
        const showCampus = routeParams.showCampus
        // if search object was set in the search component animate to this building and set the variables
        if (search) {
            animateIndoor(search.building)
            setBuilding(search.building)
            setRoom(search.room)
            setCurrentLevel(search.level)
        }
        // if campus boolean was set in the campus component animate to the campus
        if (showCampus) {
            _mapView?.animateCamera(IndoorCamera.Reset, { duration: 1000 })
            setBuilding(null)
            setRoom(null)
            setCurrentLevel(-1)
        }
    }, [routeParams])

    const getBottomSheet = (title: string, description: string) => {
        return (
            <View>
                <Text style={{ fontSize: 30 }}>{title}</Text>
                <Text style={{ fontSize: 14 }}>{description}</Text>
                <FABIconButton
                    icon="subdirectory-arrow-right"
                    onPress={() => {
                        console.log('test')
                        navigation.navigate('NavigationStack')
                    }}
                    style={{
                        position: 'absolute',
                        right: 0,
                    }}
                />
                <View
                    style={{
                        height: 3,
                        marginVertical: 25,
                        width: '100%',
                        backgroundColor: 'rgba(153, 157, 163,.2)',
                    }}
                ></View>
                <View>
                    <Text style={{ fontSize: 25 }}>
                        possible information here:
                    </Text>
                </View>
            </View>
        )
    }

    /**
     * @description:
     * animate the map to a given region.
     * possible regions are given in the constants file.
     *
     * @param polygon
     * polygon to be navigated to. Must contain a name string.
     */
    const animateIndoor = (polygon: { [key: string]: any }) => {
        _mapView?.animateCamera(IndoorCamera[polygon.name], { duration: 1000 })
    }

    const renderBuildings = () => {
        const items = []

        if (buildings) {
            for (let i = 0; i <= buildings.content.length - 1; i++) {
                items.push(renderBuilding(buildings.content[i]))
            }
        }

        return items
    }

    const renderBuilding = (building: { [key: string]: any }): ReactNode => {
        const shell = building.shell.points

        // change shells lat & lng keys to latitude & longitude, because the Polygon component need this key names
        const coordinates = shell.map((coordsArr: any) => {
            let coords = {
                latitude: coordsArr.lat,
                longitude: coordsArr.lng,
            }
            return coords
        })

        return (
            <Polygon
                fillColor="rgba(128,186,36,.7)"
                strokeColor="rgba(82, 135, 20,1)"
                tappable={true}
                onPress={() => {
                    setCurrentLevel(0)
                    setBuilding(building)
                    animateIndoor(building)
                }}
                strokeWidth={3}
                key={building.name}
                coordinates={coordinates}
            />
        )
    }

    const renderRooms = () => {
        const items = []
        for (
            let i = 0;
            i <= building.levels[currentLevel].rooms.length - 1;
            i++
        ) {
            items.push(renderRoom(building.levels[currentLevel].rooms[i], i))
        }
        return items
    }

    const renderRoom = (renderRoom: { [key: string]: any }, key: number) => {
        const shell = renderRoom.points

        // change shells lat & lng keys to latitude & longitude, because the Polygon component need this key names
        const coordinates = shell.map((coordsArr: any) => {
            let coords = {
                latitude: coordsArr.lat,
                longitude: coordsArr.lng,
            }
            return coords
        })

        return (
            <Polygon
                fillColor={
                    renderRoom.name
                        ? renderRoom.name === room?.name
                            ? 'rgba(7, 38, 4,.9)'
                            : 'rgba(128,186,36,.7)'
                        : 'rgba(128,186,36,.3)'
                }
                strokeColor="rgba(82, 135, 20,1)"
                tappable={true}
                onPress={() => {
                    if (renderRoom.name) {
                        setRoom(renderRoom)
                    }
                }}
                strokeWidth={1.5}
                key={renderRoom.name ? renderRoom.name : key}
                coordinates={coordinates}
            />
        )
    }

    const renderMap = () => {
        return (
            <>
                <View style={styles.container}>
                    <MapView
                        ref={(mapView) => {
                            _mapView = mapView
                        }}
                        initialRegion={
                            location !== 'Access Denied'
                                ? {
                                      latitude: location.coords.latitude,
                                      longitude: location.coords.longitude,
                                      latitudeDelta: 0.0000022,
                                      longitudeDelta: 0.0021,
                                  }
                                : THMCoordinates
                        }
                        scrollEnabled={currentLevel === -1 ? true : false}
                        zoomEnabled={currentLevel === -1 ? true : false}
                        rotateEnabled={currentLevel === -1 ? true : false}
                        showsUserLocation={true}
                        showsMyLocationButton={false}
                        showsPointsOfInterest={false}
                        showsCompass={false}
                        style={styles.map}
                    >
                        {
                            // if user is currently not in the indoor view (currentLevel = -1) render the building shells, else render the room shells for that building
                            currentLevel === -1
                                ? renderBuildings()
                                : renderRooms()
                        }
                    </MapView>
                </View>
                {room ? (
                    <RNBottomSheet
                        setBottomSheetOpen={(isOpen: boolean) => {
                            setIsButtomSheetOpen(isOpen)
                        }}
                        content={getBottomSheet(
                            room.name,
                            'Office / Auditorium'
                        )}
                    />
                ) : building ? (
                    <RNBottomSheet
                        setBottomSheetOpen={(isOpen: boolean) => {
                            setIsButtomSheetOpen(isOpen)
                        }}
                        content={getBottomSheet(building.name, '')}
                    />
                ) : null}
                <RNSearchbar
                    onIconPress={() => {
                        if (currentLevel >= 0) {
                            if (room) {
                                setRoom(null)
                            }
                            setCurrentLevel(-1)
                            setBuilding(null)
                            _mapView?.animateCamera(IndoorCamera.Reset, {
                                duration: 1000,
                            })
                        }
                    }}
                    onFocusBar={() => {
                        navigation.navigate('Search')
                    }}
                    onChangeText={() => {}}
                    showIcon={currentLevel >= 0 ? true : false}
                />
                {
                    /*  if user is not in indoor view display location and route button,
                    else render the level indicator except the bottom sheet is opend */
                    currentLevel === -1 ? (
                        <>
                            <FABIconButton
                                icon="map-marker"
                                onPress={() => {
                                    if (location !== 'Access Denied') {
                                        _mapView?.animateCamera(
                                            {
                                                center: {
                                                    latitude:
                                                        location.coords
                                                            .latitude,
                                                    longitude:
                                                        location.coords
                                                            .longitude,
                                                },
                                                heading: 0,
                                                pitch: 0,
                                                zoom: 17,
                                            },
                                            { duration: 1000 }
                                        )
                                    } else {
                                    }
                                }}
                                style={{
                                    position: 'absolute',
                                    margin: 16,
                                    right: 0,
                                    bottom: 100,
                                }}
                            />
                            <FABIconButton
                                icon="subdirectory-arrow-right"
                                onPress={() => {
                                    navigation.navigate('NavigationStack')
                                }}
                                style={{
                                    position: 'absolute',
                                    margin: 16,
                                    right: 0,
                                    bottom: 20,
                                }}
                            />
                        </>
                    ) : !isButtomSheetOpen ? (
                        <View
                            style={{
                                alignSelf: 'flex-end',
                                paddingTop: 50,
                                paddingRight: 20,
                            }}
                        >
                            <LevelIndicator
                                levels={building.levels.length - 1}
                                startLevel={currentLevel}
                                changeLevel={(buttonLevel: number) => {
                                    setCurrentLevel(buttonLevel)
                                }}
                            />
                        </View>
                    ) : null
                }
            </>
        )
    }

    return buildings && location ? renderMap() : <Loading />
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        position: 'absolute',
    },
    map: {
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        position: 'absolute',
    },
})
