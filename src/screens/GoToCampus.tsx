import React, { useEffect } from 'react'
import { MapTabsNavProps } from '../routes/mapTabs/MapTabsParamList'
import { useIsFocused } from '@react-navigation/native'
import { Loading } from '../components/Loading'

/**
 * This component is called whenever the user tabs the Campus icon in the BottomNavigation.
 *
 * If it's called it redirects the user back to the map component, with the instruction to animate to the campus.
 */
export const GoToCampus = ({ navigation }: MapTabsNavProps<'Campus'>) => {
    const isFocused = useIsFocused()

    useEffect(() => {
        navigation.navigate('Map', { showCampus: true })
    }, [isFocused])

    return <Loading></Loading>
}
