// Coordinates of the THM Campus in Gießen
export var THMCoordinates = {
    latitude: 50.586995,
    longitude: 8.68232,
    latitudeDelta: 0.0000022,
    longitudeDelta: 0.0021,
}

export var THMColors = {
    green: '#80ba24',
    gray: '#4a5c66',
}

// List of the Camera Positions for every Room and Building
export var IndoorCamera = {
    A20: {
        center: {
            latitude: 50.58707862061302,
            longitude: 8.681895174086094,
        },
        heading: 46.62314987182617,
        pitch: 0,
        zoom: 19.052541732788086,
    },
    A12: {
        center: {
            latitude: 50.58673206956566,
            longitude: 8.682595901191235,
        },
        heading: 333.7832946777344,
        pitch: 0,
        zoom: 19.477657318115234,
    },
    Reset: {
        center: {
            latitude: 50.587008799722675,
            longitude: 8.682322315871716,
        },
        heading: 0,
        pitch: 0,
        zoom: 17.878984451293945,
    },
    'A20.0.7': {
        center: {
            latitude: 50.58720314846838,
            longitude: 8.681943118572235,
        },
        heading: 56.84168243408203,
        pitch: 0,
        zoom: 20.166521072387695,
    },
    'A20.0.8': {
        center: {
            latitude: 50.58726360289823,
            longitude: 8.68207823485136,
        },
        heading: 56.84168243408203,
        pitch: 0,
        zoom: 20.166521072387695,
    },
    'A20.0.9': {
        center: {
            latitude: 50.587299790371894,
            longitude: 8.68217680603266,
        },
        heading: 56.84168243408203,
        pitch: 0,
        zoom: 20.166521072387695,
    },
    'Lift A20': {
        center: {
            latitude: 50.58703349248839,
            longitude: 8.681719154119492,
        },
        heading: 56.84168243408203,
        pitch: 0,
        zoom: 20.166521072387695,
    },
    Foyer: {
        center: {
            latitude: 50.58701582439009,
            longitude: 8.681658133864403,
        },
        heading: 56.84168243408203,
        pitch: 0,
        zoom: 20.166521072387695,
    },
    'Cafeteria Campus Tor': {
        center: {
            latitude: 50.58692514223902,
            longitude: 8.681579679250717,
        },
        heading: 66.05110168457031,
        pitch: 0,
        zoom: 19.888696670532227,
    },
    'A20.1.36': {
        center: {
            latitude: 50.58692514223902,
            longitude: 8.681579679250717,
        },
        heading: 66.05110168457031,
        pitch: 0,
        zoom: 19.888696670532227,
    },
    'A20.1.7': {
        center: {
            latitude: 50.58720314846838,
            longitude: 8.681943118572235,
        },
        heading: 56.84168243408203,
        pitch: 0,
        zoom: 20.166521072387695,
    },
    'A20.1.8': {
        center: {
            latitude: 50.58726360289823,
            longitude: 8.68207823485136,
        },
        heading: 56.84168243408203,
        pitch: 0,
        zoom: 20.166521072387695,
    },
    'A20.1.9': {
        center: {
            latitude: 50.587299790371894,
            longitude: 8.68217680603266,
        },
        heading: 56.84168243408203,
        pitch: 0,
        zoom: 20.166521072387695,
    },
    'A20.1.10': {
        center: {
            latitude: 50.58724316760668,
            longitude: 8.682217709720135,
        },
        heading: 55.601985931396484,
        pitch: 0,
        zoom: 20.058963775634766,
    },
    'Lift A12': {
        center: {
            latitude: 50.5868231778277,
            longitude: 8.682543933391571,
        },
        heading: 333.0077209472656,
        pitch: 0,
        zoom: 20.704866409301758,
    },
}
