import React, { useState } from 'react'
import { AsyncStorage } from 'react-native'

type Person = null | { [key: string]: any }

export const PersonContext = React.createContext<{
    persons: Person
    getPersons: () => void
}>({
    persons: null,
    getPersons: () => {},
})

interface PersonProviderProps {}

/**
 * PersonProvider component
 *
 * @description:
 * acquieres the person data and saves it, to make is accessable for the whole application
 *
 * @var:
 * getPersons: void => can be called to acquire the data from the backend
 * persons: Person => data acquired during getPersons call is saved here
 */
export const PersonProvider: React.FC<PersonProviderProps> = ({ children }) => {
    const [persons, setPersons] = useState(null)
    var errMsg = ''

    return (
        <PersonContext.Provider
            value={{
                persons,
                getPersons: () => {
                    fetch('https://mocainfo.thm.de/backend/persons')
                        .then((response) => response.json())
                        .then((response) => {
                            setPersons(response)
                            AsyncStorage.setItem('persons', response).catch(
                                () => {
                                    errMsg = 'unable to set person data'
                                }
                            )
                        })
                        .catch(() => {
                            errMsg = 'unable to access person data'
                        })
                },
            }}
        >
            {children}
        </PersonContext.Provider>
    )
}
