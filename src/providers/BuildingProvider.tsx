import React, { useState } from 'react'
import { AsyncStorage } from 'react-native'

type Building = null | { [key: string]: any }

export const BuildingContext = React.createContext<{
    buildings: Building
    getBuildings: () => void
}>({
    buildings: null,
    getBuildings: () => {},
})

/**
 * BuildingProvider component
 *
 * @description:
 * acquieres the building data and saves it, to make is accessable for the whole application
 *
 * @var:
 * getBuildings: void => can be called to acquire the data from the backend
 * buildings: Building => data acquired during getBuildings call is saved here
 */
export const BuildingProvider: React.FC = ({ children }) => {
    const [buildings, setBuildings] = useState(null)
    var errMsg = ''

    return (
        <BuildingContext.Provider
            value={{
                buildings,
                getBuildings: () => {
                    fetch(
                        'https://mocainfo.thm.de/indoor-model/api/v2/buildings?withLevels=true'
                    )
                        .then((response) => response.json())
                        .then((response) => {
                            setBuildings(response.buildings)
                            AsyncStorage.setItem(
                                'buildings',
                                response.buildings
                            ).catch(() => {
                                errMsg = 'unable to set building data'
                            })
                        })
                        .catch(() => {
                            errMsg = 'unable to access building data'
                        })
                },
            }}
        >
            {children}
        </BuildingContext.Provider>
    )
}
