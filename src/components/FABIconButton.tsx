import React from 'react'
import { StyleSheet } from 'react-native'
import { FAB } from 'react-native-paper'
import { THMColors } from '../constants/constants'

interface FABIconButtonProps {
    icon: string
    iconColor?: string
    style?: any
    onPress(): void
}

/**
 * @description
 * customized Icon Button from React Native Paper
 */
export const FABIconButton = (props: FABIconButtonProps) => {
    const { icon, iconColor, style, onPress } = props
    const iconColorProp = iconColor ? iconColor : 'black'
    const styleProp = style ? style : styles.fab

    return (
        <FAB
            style={styleProp}
            theme={{ colors: { accent: THMColors.green } }}
            color={iconColorProp}
            icon={icon}
            onPress={() => onPress()}
        />
    )
}

const styles = StyleSheet.create({
    fab: {
        position: 'absolute',
        margin: 16,
        right: 0,
        bottom: 0,
    },
})
