import React, { useRef } from 'react'
import BottomSheet from 'reanimated-bottom-sheet'
import { View, StyleSheet } from 'react-native'
import Animated from 'react-native-reanimated'

/*
    The library 'reanimated-bootom-sheet' is used in this application to display BottomSheets.
    It is the recomended library by 'react-native-paper'.

    https://github.com/osdnk/react-native-reanimated-bottom-sheet
*/

interface RNBottomSheetProps {
    content: React.ReactNode
    setBottomSheetOpen(isOpen: boolean): void
}

export const RNBottomSheet = (props: RNBottomSheetProps) => {
    const { content, setBottomSheetOpen } = props
    const [position, setPoistion] = React.useState(0.6)

    const bottomSheetRef = useRef<BottomSheet | null>(null)
    const callbackNode = useRef(new Animated.Value(1))

    /**
     *  Used to get information in which state the BottomSheet currently is.
     *  There is a callback everytime the size of the sheet changes.
     *  The current size is saved in 'value' and is between 0 (completly open) and 1 (completly closed).
     *
     *  Is 1 - the value > 0.6 the 'setBottomSheetOpen' function is called with true.
     *  If not it will be called with false.
     *
     *  Parts of this code quotes a solution to an issue on the GitHub page: (https://github.com/osdnk/react-native-reanimated-bottom-sheet/issues/71)
     */
    Animated.useCode(
        () =>
            Animated.onChange(
                callbackNode.current,
                Animated.call([callbackNode.current], (value) => {
                    setPoistion(1 - value)
                    1 - value > 0.6
                        ? setBottomSheetOpen(true)
                        : setBottomSheetOpen(false)
                })
            ),
        []
    )

    return (
        <BottomSheet
            ref={bottomSheetRef}
            callbackNode={callbackNode.current}
            enabledContentTapInteraction={false}
            initialSnap={1}
            snapPoints={['100%', '18%', 0]}
            renderHeader={() => (
                <View style={styles.sheetHeader}>
                    <View style={styles.panelHeader}>
                        <View style={styles.panelBar} />
                    </View>
                </View>
            )}
            renderContent={() => (
                <View style={styles.sheetContent}>
                    <View
                        style={{
                            height: '100%',
                            marginTop: position > 0.6 ? 110 : 0,
                        }}
                    >
                        {content}
                    </View>
                </View>
            )}
        />
    )
}

const styles = StyleSheet.create({
    modal: {
        margin: 0,
    },
    container: {
        flex: 1,
    },
    sheetHeader: {
        backgroundColor: '#fff',
        paddingTop: 20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        borderColor: 'lightgray',
        borderWidth: 1,
        borderBottomWidth: 0,
    },
    sheetContent: {
        borderColor: 'lightgray',
        borderWidth: 1,
        borderTopWidth: 0,
        backgroundColor: '#fff',
        padding: 15,
    },
    panelHeader: {
        alignItems: 'center',
    },
    panelBar: {
        width: 40,
        height: 8,
        borderRadius: 4,
        backgroundColor: '#00000040',
        marginBottom: 10,
    },
})
