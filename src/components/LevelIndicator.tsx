import React from 'react'
import { View } from 'react-native'
import { Button } from 'react-native-paper'

interface LevelIndicatorProps {
    levels: number
    startLevel: number
    changeLevel(buttonLevel: number): void
}

export const LevelIndicator = (props: LevelIndicatorProps) => {
    const { levels, startLevel, changeLevel } = props
    const [currentLevel, setCurrentLevel] = React.useState(startLevel)

    const renderButtons = () => {
        const items = []

        for (let i = 0; i <= levels; i++) {
            items.push(renderButton(i))
        }

        return items
    }

    const renderButton = (buttonLevel: number) => {
        return (
            <Button
                style={
                    buttonLevel === currentLevel
                        ? {
                              height: 40,
                              width: 40,
                              marginBottom: 0,
                              borderColor: 'lightgray',
                              borderTopWidth: buttonLevel != 0 ? 0.75 : 0,
                          }
                        : {
                              backgroundColor: 'white',
                              height: 40,
                              width: 40,
                              marginBottom: 0,
                              borderColor: 'lightgray',
                              borderTopWidth: buttonLevel != 0 ? 0.75 : 0,
                          }
                }
                theme={{ roundness: 0 }}
                key={buttonLevel}
                compact={true}
                mode="contained"
                onPress={() => {
                    if (buttonLevel !== currentLevel) {
                        setCurrentLevel(buttonLevel)
                        changeLevel(buttonLevel)
                    }
                }}
            >
                {buttonLevel}
            </Button>
        )
    }

    return <View>{renderButtons()}</View>
}
