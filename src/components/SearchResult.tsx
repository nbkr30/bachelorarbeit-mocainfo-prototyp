import React from 'react'
import { List, Surface } from 'react-native-paper'

interface SearchResultProps {
    result: any
    onPress(): void
}

export const SearchResult = (props: SearchResultProps) => {
    const { result, onPress } = props

    var description, title, icon: string

    // sets the variables depending on the type of the result because every type has differents paths and icons
    if (result.type === 'room') {
        title = result.room.name
        description = result.room.properties.description
            ? result.room.properties.description
            : ''
        icon = 'door'
    } else if (result.type === 'building') {
        title = result.building.name
        description = ''
        icon = 'office-building'
    } else if (result.type === 'person') {
        title = result.person.name
        description = ''
        icon = 'account'
    }

    return (
        <Surface
            style={{
                elevation: 4,
                margin: 1,
            }}
        >
            <List.Item
                onPress={() => {
                    onPress()
                }}
                left={(props) => <List.Icon {...props} icon={icon} />}
                title={title}
                description={description}
            />
        </Surface>
    )
}
