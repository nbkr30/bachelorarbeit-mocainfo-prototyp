import React, { useRef } from 'react'
import { View } from 'react-native'
import { Searchbar } from 'react-native-paper'

interface RNSearchbarProps {
    isFocused?: boolean
    onIconPress(): void
    onFocusBar(): void
    onChangeText(value: string): void
    showIcon?: boolean
}

export const RNSearchbar = (props: RNSearchbarProps) => {
    const { isFocused, onIconPress, onFocusBar, showIcon, onChangeText } = props
    const [searchQuery, setSearchQuery] = React.useState('')
    const searchBarReference = useRef(null)

    const onChangeSearch = (query: string) => {
        setSearchQuery(query)
        onChangeText(query)
    }

    return (
        <>
            <View
                style={{
                    marginTop: 50,
                    paddingHorizontal: 5,
                }}
            >
                <Searchbar
                    ref={searchBarReference}
                    autoFocus={isFocused ? isFocused : false}
                    placeholder="Search..."
                    value={searchQuery}
                    icon="arrow-left"
                    iconColor={showIcon ? undefined : 'white'}
                    onFocus={() => {
                        onFocusBar()
                    }}
                    onChangeText={(query: string) => {
                        onChangeSearch(query)
                    }}
                    onIconPress={() => onIconPress()}
                />
            </View>
        </>
    )
}
