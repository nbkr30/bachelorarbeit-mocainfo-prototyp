import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { Text, View } from 'react-native'

interface QrStackProps {}

function Qr() {
    return (
        <View
            style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
        >
            <Text>Qr-Reader dummy screen</Text>
        </View>
    )
}

const Stack = createStackNavigator()

export const QrStack: React.FC<QrStackProps> = ({}) => {
    return (
        <Stack.Navigator
            initialRouteName="Timetable"
            screenOptions={{ header: () => null }}
        >
            <Stack.Screen name="QR-Code Scanner" component={Qr} />
        </Stack.Navigator>
    )
}
