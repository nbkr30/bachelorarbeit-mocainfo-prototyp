import { RouteProp } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack'

export type MainStackParamList = {
    MapTabs: {
        search: { [key: string]: any }
        showCampus: boolean
    }
    NavigationStack: undefined
    Search: undefined
}

export type MainStackNavProps<T extends keyof MainStackParamList> = {
    navigation: StackNavigationProp<MainStackParamList, T>
    route: RouteProp<MainStackParamList, T>
}
