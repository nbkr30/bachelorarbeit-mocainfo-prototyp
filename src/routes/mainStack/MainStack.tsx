import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { MapTabs } from '../mapTabs/MapTabs'
import { NavigationStack } from '../navigationStack/NavigationStack'
import { MainStackParamList } from './MainStackParamList'
import { Search } from '../../screens/Search'

interface MainStackProps {}

const Stack = createStackNavigator<MainStackParamList>()

export const MainStack: React.FC<MainStackProps> = ({}) => {
    return (
        <Stack.Navigator
            initialRouteName="MapTabs"
            screenOptions={{ header: () => null }}
        >
            <Stack.Screen name="MapTabs" component={MapTabs} />
            <Stack.Screen name="NavigationStack" component={NavigationStack} />
            <Stack.Screen name="Search" component={Search} />
        </Stack.Navigator>
    )
}
