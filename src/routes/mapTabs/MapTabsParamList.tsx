import { RouteProp } from '@react-navigation/native'
import { MaterialBottomTabNavigationProp } from '@react-navigation/material-bottom-tabs'

export type MapTabsParamList = {
    Timetable: undefined
    Map: {}
    Campus: undefined
    'QR-Code': undefined
    '360° View': {}
}

export type MapTabsNavProps<T extends keyof MapTabsParamList> = {
    navigation: MaterialBottomTabNavigationProp<MapTabsParamList, T>
    route: RouteProp<MapTabsParamList, T>
}
