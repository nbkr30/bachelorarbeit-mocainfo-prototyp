import React from 'react'
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs'
import { THMColors } from '../../constants/constants'
import { TimetableStack } from '../TimetableStack'
import { QrStack } from '../QrStack'
import { MapTabsParamList } from './MapTabsParamList'
import { Map } from '../../screens/Map'
import { GoToCampus } from '../../screens/GoToCampus'

interface MapTabsProps {}

const Tabs = createMaterialBottomTabNavigator<MapTabsParamList>()

export const MapTabs: React.FC<MapTabsProps> = ({}) => {
    return (
        <Tabs.Navigator
            initialRouteName="Map"
            barStyle={{ backgroundColor: THMColors.green }}
            shifting={false}
        >
            <Tabs.Screen
                options={{ tabBarIcon: 'home-map-marker' }}
                name="Map"
                initialParams={{ showCampus: false, searchedRoom: null }}
                component={Map}
            />
            <Tabs.Screen
                options={{ tabBarIcon: 'timetable' }}
                name="Timetable"
                component={TimetableStack}
            />
            <Tabs.Screen
                options={{ tabBarIcon: 'qrcode-scan' }}
                name="QR-Code"
                component={QrStack}
            />
            <Tabs.Screen
                options={{ tabBarIcon: 'camera' }}
                name="360° View"
                initialParams={{ showCampus: false, searchedRoom: null }}
                component={Map}
            />
            <Tabs.Screen
                options={{ tabBarIcon: 'office-building' }}
                name="Campus"
                component={GoToCampus}
            />
        </Tabs.Navigator>
    )
}
