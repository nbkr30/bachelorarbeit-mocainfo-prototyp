import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { SearchStackParamList } from './SearchStackParamList'
import { Search } from '../../screens/Search'

interface SearchStackProps {}

const Stack = createStackNavigator<SearchStackParamList>()

export const SearchStack: React.FC<SearchStackProps> = ({}) => {
    return (
        <Stack.Navigator
            initialRouteName="Search"
            screenOptions={{ header: () => null }}
        >
            <Stack.Screen name="Search" component={Search} />
        </Stack.Navigator>
    )
}
