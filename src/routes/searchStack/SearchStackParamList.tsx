import { RouteProp } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack'

export type SearchStackParamList = {
    Search: undefined
}

export type SearchStackNavProps<T extends keyof SearchStackParamList> = {
    navigation: StackNavigationProp<SearchStackParamList, T>
    route: RouteProp<SearchStackParamList, T>
}
