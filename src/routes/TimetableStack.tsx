import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { Text, View } from 'react-native'

interface TimetableStackProps {}

function Timetable() {
    return (
        <View
            style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
        >
            <Text>Timetable dummy screen</Text>
        </View>
    )
}

const Stack = createStackNavigator()

export const TimetableStack: React.FC<TimetableStackProps> = ({}) => {
    return (
        <Stack.Navigator
            initialRouteName="Timetable"
            screenOptions={{ header: () => null }}
        >
            <Stack.Screen name="Timetable" component={Timetable} />
        </Stack.Navigator>
    )
}
