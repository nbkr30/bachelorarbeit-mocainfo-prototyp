import { RouteProp } from '@react-navigation/native'
import { StackNavigationProp } from '@react-navigation/stack'

export type NavigationStackParamList = {
    Navigation: undefined
}

export type NavigationStackNavProps<
    T extends keyof NavigationStackParamList
> = {
    navigation: StackNavigationProp<NavigationStackParamList, T>
    route: RouteProp<NavigationStackParamList, T>
}
