import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { Text, View } from 'react-native'
import { NavigationStackParamList } from './NavigationStackParamList'

interface NavigationStackProps {}

function Navigation() {
    return (
        <View
            style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
        >
            <Text>Navigation dummy screen</Text>
        </View>
    )
}

const Stack = createStackNavigator<NavigationStackParamList>()

export const NavigationStack: React.FC<NavigationStackProps> = ({}) => {
    return (
        <Stack.Navigator
            initialRouteName="Navigation"
            screenOptions={{ header: () => null }}
        >
            <Stack.Screen name="Navigation" component={Navigation} />
        </Stack.Navigator>
    )
}
