import React from 'react'
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper'
import { NavigationContainer } from '@react-navigation/native'
import { THMColors } from './src/constants/constants'
import { MainStack } from './src/routes/mainStack/MainStack'
import { BuildingProvider } from './src/providers/BuildingProvider'
import { PersonProvider } from './src/providers/PersonProvider'

/*
This project uses different librarys to achieve important functionalities:

React Native Paper:

    The React Native Paper library is used to achieve components which are following the material guidelines.
    https://callstack.github.io/react-native-paper/getting-started.html

React Navigation:

    React Navigation is used to make a multiscreen Application.
    https://reactnavigation.org/
    To get into React Navigation a tutorial by Ben Awad was used: https://www.youtube.com/watch?v=Hln37dE19bs

React Native Maps

    React Native Maps is used to display the map, to highlight the buildings and the user location.
    https://github.com/react-native-community/react-native-maps
    
Reanimated Bottom Sheet

    The library 'reanimated-bootom-sheet' is used in this application to display BottomSheets.
    It is the recomended library by 'react-native-paper'.
    https://github.com/osdnk/react-native-reanimated-bottom-sheet
*/

// this theme is used in the whole application
const theme = {
    ...DefaultTheme,
    colors: {
        ...DefaultTheme.colors,
        primary: THMColors.green,
        accent: 'yellow',
    },
}

export default function App() {
    return (
        <PaperProvider theme={theme}>
            <BuildingProvider>
                <PersonProvider>
                    <NavigationContainer>
                        <MainStack />
                    </NavigationContainer>
                </PersonProvider>
            </BuildingProvider>
        </PaperProvider>
    )
}
